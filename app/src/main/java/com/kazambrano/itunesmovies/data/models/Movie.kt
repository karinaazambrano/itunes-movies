package com.kazambrano.itunesmovies.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "movie")
@Parcelize
data class Movie(
    @PrimaryKey val trackId: Int,
    val trackName : String,
    val artworkUrl100 : String,
    val trackPrice : Double?,
    val currency: String?,
    val primaryGenreName : String?,
    val longDescription : String?
) : Parcelable