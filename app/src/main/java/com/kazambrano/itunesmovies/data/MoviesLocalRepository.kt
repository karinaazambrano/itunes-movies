package com.kazambrano.itunesmovies.data

import androidx.lifecycle.LiveData
import com.kazambrano.itunesmovies.data.models.Movie
import javax.inject.Inject

class MoviesLocalRepository @Inject constructor(private val moviesLocalDao: MoviesLocalDao) {
    suspend fun insert(movie: Movie) = moviesLocalDao.insert(movie)
    val allMovies : LiveData<List<Movie>> = moviesLocalDao.getAllMovies()
}