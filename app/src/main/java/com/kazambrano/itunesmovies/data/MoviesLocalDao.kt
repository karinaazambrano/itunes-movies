package com.kazambrano.itunesmovies.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kazambrano.itunesmovies.data.models.Movie

@Dao
interface MoviesLocalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: Movie)

    @Query("SELECT * FROM movie ORDER BY trackName ASC")
    fun getAllMovies() : LiveData<List<Movie>>
}