package com.kazambrano.itunesmovies.data

import com.kazambrano.itunesmovies.data.models.ListenMoviesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesRemoteDao {//https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all
    @GET("search?")
    suspend fun listenMovies(
        @Query("term") term: String? = "star",
        @Query("country") country: String? = "au",
        @Query("media") media: String? = "movie"
    ) : ListenMoviesResponse
}