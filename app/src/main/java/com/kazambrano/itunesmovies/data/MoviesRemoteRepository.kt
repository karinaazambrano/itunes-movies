package com.kazambrano.itunesmovies.data

import com.kazambrano.itunesmovies.data.models.Movie
import javax.inject.Inject

class MoviesRemoteRepository @Inject constructor(private val remoteDAO: MoviesRemoteDao){
    suspend fun getMovies() : List<Movie>? = remoteDAO.listenMovies().results
}