package com.kazambrano.itunesmovies.data.models

import com.google.gson.annotations.SerializedName

data class ListenMoviesResponse(

	@field:SerializedName("resultCount")
	val resultCount: Int? = null,

	@field:SerializedName("results")
	val results: List<Movie>? = null
)