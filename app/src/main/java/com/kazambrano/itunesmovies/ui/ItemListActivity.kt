package com.kazambrano.itunesmovies.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kazambrano.itunesmovies.R
import com.kazambrano.itunesmovies.data.models.Movie
import com.kazambrano.itunesmovies.databinding.ActivityItemListBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [ItemDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
@AndroidEntryPoint
class ItemListActivity : AppCompatActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private val movieListViewModel : MovieListViewModel by viewModels()
    private lateinit var adapter: MovieListAdapter
    private lateinit var binding: ActivityItemListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = binding.toolbar
        setSupportActionBar(toolbar)
        toolbar.title = title

        if (findViewById<NestedScrollView>(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }
        //GET REFERENCE FOR EASY ACCESS
        val recyclerView: RecyclerView = binding.include.itemList

        //SET ADAPTER
        adapter = MovieListAdapter(this, onClickListener)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        //START DOWNLOAD OF ALBUMS
        movieListViewModel.downloadMovies()

        //LISTEN TO STATUS OF DOWNLOAD AND DISPLAY APPROPRIATE VIEW
        movieListViewModel.isLoading.observe(this, Observer { isLoading ->
            if(!isLoading) {
                binding.progressBar.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            }
            else {
                binding.progressBar.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }
        })

        //SET DATA TO ADAPTER
        movieListViewModel.allMovies.observe(this, Observer { movies ->
            adapter.setMovies(movies)
            binding.message.visibility = if(movies.isEmpty()) View.VISIBLE else View.GONE
        })
    }

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Movie
            if (twoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ItemDetailFragment.ARG_MOVIE, item)
                    }
                }
                this.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                    putExtra(ItemDetailFragment.ARG_MOVIE, item)
                }
                v.context.startActivity(intent)
            }
        }
    }
}