package com.kazambrano.itunesmovies.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kazambrano.itunesmovies.R
import com.kazambrano.itunesmovies.data.models.Movie
import com.kazambrano.itunesmovies.databinding.ItemListContentBinding

class MovieListAdapter constructor(val context: Context, val listener: View.OnClickListener) : RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>() {
    private var movies = emptyList<Movie>()
    inner class MovieViewHolder(val binding: ItemListContentBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListContentBinding.inflate(inflater, parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val current = movies[position]
        holder.binding.run {
            movie = current
            root.tag = current
            root.setOnClickListener(listener)
        }
    }

    override fun getItemCount() = movies.size

    fun setMovies(movies: List<Movie>)
    {
        this.movies = movies
        notifyDataSetChanged()
    }

    companion object {

        //SET IMAGE ALBUM
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String) {
            if (url.isNotEmpty())
                Glide.with(view)
                    .load(url)
                    .placeholder(R.drawable.baseline_photo_black_24)
                    .override(600, 400)
                    .into(view)
        }
    }
}