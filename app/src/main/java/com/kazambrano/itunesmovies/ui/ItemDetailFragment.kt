package com.kazambrano.itunesmovies.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.CollapsingToolbarLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.kazambrano.itunesmovies.R
import com.kazambrano.itunesmovies.data.models.Movie
import com.kazambrano.itunesmovies.databinding.ItemDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItemDetailFragment : Fragment() {

    private var movie: Movie? = null
    private lateinit var binding: ItemDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_MOVIE)) {
                movie = it.getParcelable(ARG_MOVIE)
                activity?.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)?.title =
                    movie?.trackName
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.item_detail, container, false)
        movie?.let {
            binding.movie = it
        }
        return binding.root
    }

    companion object {
        const val ARG_MOVIE = "movie"
        //SET ALBUM IMAGE
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String) {
            if (url.isNotEmpty())
                Glide.with(view)
                    .load(url)
                    .placeholder(R.drawable.baseline_photo_black_48)
                    .override(600, 400)
                    .into(view)
        }
    }
}