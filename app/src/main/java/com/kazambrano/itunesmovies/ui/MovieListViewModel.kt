package com.kazambrano.itunesmovies.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.kazambrano.itunesmovies.data.models.Movie
import com.kazambrano.itunesmovies.data.MoviesLocalRepository
import com.kazambrano.itunesmovies.data.MoviesRemoteRepository
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception


class MovieListViewModel @ViewModelInject constructor(
    val remoteRepository: MoviesRemoteRepository,
    val localRepository: MoviesLocalRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel()
{
    //GET REFERENCE TO LIVEDATA FROM REPOSITORY
    val allMovies : LiveData<List<Movie>> =  localRepository.allMovies

    //CHECK STATUS OF DOWNLOAD TO DECIDE VISIBILITY OF THE LIST
    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading : LiveData<Boolean> = _isLoading

    fun downloadMovies() =
        viewModelScope.launch {
            _isLoading.value = true
            try {
                val result = remoteRepository.getMovies()
                //save
                if(result!=null)
                for(movie in result)
                {
                    localRepository.insert(movie)
                }
            } catch (e: Exception)
            {
                Timber.e(e.toString())
            } finally {
                _isLoading.value = false
            }
        }

}