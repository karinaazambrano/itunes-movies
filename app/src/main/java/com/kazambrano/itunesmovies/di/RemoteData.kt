package com.kazambrano.itunesmovies.di

import com.kazambrano.itunesmovies.Config
import com.kazambrano.itunesmovies.data.MoviesRemoteDao
import com.kazambrano.itunesmovies.data.MoviesRemoteRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RemoteData {

    @Provides
    @Singleton
    fun provideMovieService() : MoviesRemoteDao = Retrofit.Builder()
        .baseUrl(Config.API_ENDPOINT)
        .client(
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(2, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(MoviesRemoteDao::class.java)

    @Provides
    @Singleton
    fun provideMovieRemoteRepository(dao: MoviesRemoteDao) = MoviesRemoteRepository(dao)
}