package com.kazambrano.itunesmovies.di

import android.content.Context
import androidx.room.Room
import com.kazambrano.itunesmovies.MovieRoomDatabase
import com.kazambrano.itunesmovies.data.MoviesLocalDao
import com.kazambrano.itunesmovies.data.MoviesLocalRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object LocalData {

    @Singleton
    @Provides
    fun provideLocalDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, MovieRoomDatabase::class.java, "movie_database").build()

    @Singleton
    @Provides
    fun provideMovieLocalDao(db: MovieRoomDatabase) = db.movieDao()

    @Singleton
    @Provides
    fun provideMovieLocalRepository(dao: MoviesLocalDao) = MoviesLocalRepository(dao)
}