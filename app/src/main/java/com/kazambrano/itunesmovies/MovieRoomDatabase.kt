package com.kazambrano.itunesmovies

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kazambrano.itunesmovies.data.models.Movie
import com.kazambrano.itunesmovies.data.MoviesLocalDao

@Database(entities = arrayOf(Movie::class), version = 2, exportSchema = false)
public abstract class MovieRoomDatabase : RoomDatabase()
{
    abstract fun movieDao(): MoviesLocalDao

/*    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: MovieRoomDatabase? = null

        fun getDatabase(@ApplicationContext context: Context): MovieRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    MovieRoomDatabase::class.java,
                    "movie_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }*/
}